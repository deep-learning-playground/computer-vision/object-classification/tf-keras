# TF-Keras

* Augmentation using Albumentations library
* Modular code
* 


# Methods
* Following are the ways to build a data pipeline:
	1. Using tf.keras.preprocessing.image.ImageDataGenerator
	2. Using tf.keras.preprocessing.image_dataset_from_directory
	3. Using tf.Data.dataset

### Custom defined metrics using Keras backend
* https://datascience.stackexchange.com/questions/45165/how-to-get-accuracy-f1-precision-and-recall-for-a-keras-model


### Path(s) to dataset(train, validation, test)
* dataset split using: https://pypi.org/project/split-folders/

### Load dataset
* using image_dataset_from_directory from keras preprocessing module
* using tf.data.Dataset.list_files which converts files to tf.Dataset objects
* API reference: https://www.tensorflow.org/tutorials/load_data/images


### Configure dataset for performance
* Used TF API Reference: https://www.tensorflow.org/tutorials/load_data/images#configure_dataset_for_performance

### Custom focal loss function
* to handle class imbalance in the dataset
* code courtesy: https://gist.github.com/Tony607/a8699bdfdac5aa2327c2582544dff2e9#file-focal-loss-model-py
* an official implementation of focal loss can also be found at: https://www.tensorflow.org/addons/api_docs/python/tfa/losses



## Doubts
1. https://www.tensorflow.org/tutorials/load_data/images
2. Build model without the augmentation layer: https://keras.io/guides/transfer_learning/
3. Using functools partial to provide parameters to tf.data.dataset map function: https://stackoverflow.com/questions/46263963/how-to-map-a-function-with-additional-parameter-using-the-new-dataset-api-in-tf1
4. Keras preprocess_input vs image normalization by 255: https://stackoverflow.com/questions/54702212/keras-rescale-1-255-vs-preprocessing-function-preprocess-input-which-one-to
5. using preprocess input as a part of model layer: https://keras.io/examples/keras_recipes/tfrecord/

# References
1. https://stackoverflow.com/questions/53514495/what-does-batch-repeat-and-shuffle-do-with-tensorflow-dataset
2. https://cs230.stanford.edu/blog/datapipeline/#building-an-image-data-pipeline
3. https://towardsdatascience.com/what-is-the-best-input-pipeline-to-train-image-classification-models-with-tf-keras-eb3fe26d3cc5
4. https://stackoverflow.com/questions/54281719/how-to-get-code-completion-in-a-jupyter-notebook-running-in-docker
5. https://www.tensorflow.org/addons

