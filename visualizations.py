from itertools import cycle
import pandas as pd
import matplotlib.pyplot as plt

from sklearn.metrics import classification_report, confusion_matrix, roc_curve, auc
from sklearn.metrics import confusion_matrix, f1_score, precision_score, recall_score, roc_auc_score

# import plotly
# plotly.offline.init_notebook_mode(connected=True) 

import plotly.express as px
import plotly.io as pio
import plotly.graph_objects as go
'''
https://plotly.com/python/renderers/

 Available renderers:
        ['plotly_mimetype', 'jupyterlab', 'nteract', 'vscode',
         'notebook', 'notebook_connected', 'kaggle', 'azure', 'colab',
         'cocalc', 'databricks', 'json', 'png', 'jpeg', 'jpg', 'svg',
         'pdf', 'browser', 'firefox', 'chrome', 'chromium', 'iframe',
         'iframe_connected', 'sphinx_gallery', 'sphinx_gallery_png']
'''

# pio.renderers.default='notebook'





def plot_history_accuracy(history_df, renderer):
	pio.renderers.default=renderer
	
	fig = px.line(history_df[['categorical_accuracy', 'val_categorical_accuracy']], 
	          labels={'index':'Epochs', 'value':'Accuracy', 'variable':'Categorical Accuracy'},
	          title="Model Accuracy",

	         )
	names = cycle(['Training', 'Validation'])
	fig.for_each_trace(lambda t:  t.update(name = next(names)))
	fig.show()



def plt_plot_accuracy(history):
    # plt.figure(1, figsize=(15, 5))
    # summarize history for accuracy
    # plt.subplot(211)
    plt.figure(figsize=(15, 5))
    plt.plot(history['categorical_accuracy'])
    plt.plot(history['val_categorical_accuracy'])
    plt.title('Model Accuracy')
    plt.ylabel('Accuracy')
    plt.xlabel('Epoch')
    plt.legend(['Training', 'Validation'], loc='lower right')
    plt.tight_layout()
    
    plt.show()


def plot_history_loss(history_df, renderer):
	pio.renderers.default=renderer

	fig = px.line(history_df[['loss', 'val_loss']], 
	       labels={'index':'Epochs', 'value':'Loss', 'variable':'Loss'},
	       title="Model Loss",

	      )
	names = cycle(['Training', 'Validation'])
	fig.for_each_trace(lambda t:  t.update(name = next(names)))
	fig.show()


def plt_plot_loss(history):
    #plt.figure(2, figsize=(15, 5))
    # summarize history for loss
    # plt.subplot(212)
    plt.figure(figsize=(15, 5))
    plt.plot(history['loss'])
    plt.plot(history['val_loss'])
    plt.title('Model Loss')
    plt.ylabel('Loss')
    plt.xlabel('Epoch')
    plt.legend(['Training', 'Validation'], loc='upper right')
    plt.tight_layout()
    plt.show()


def plot_confusion_matrix(confusion_matrix, class_names, renderer, size=950):
    fig = px.imshow(pd.DataFrame(confusion_matrix, columns=class_names), x=class_names, y=class_names)
    fig.update_layout(
        autosize=False,
        width=size,
        height=size,)
    fig.show(renderer)


def plot_roc_curve(y_true_df, y_pred_df, renderer, size=950):

	# this is how it was done initially
	# y_true = tensorflow.keras.utils.to_categorical(true_classes)
	# y_pred_oh = tensorflow.keras.utils.to_categorical(predicted_classes)
	# y_true_df = pd.DataFrame(y_true, columns=CLASS_NAMES)

    # Create an empty figure, and iteratively add new lines
    # every time we compute a new class
    fig = go.Figure()
    fig.add_shape(type='line', line=dict(dash='dash'),x0=0, x1=1, y0=0, y1=1)


    for i in range(y_pred_df.shape[1]):
        y_true_s = y_true_df.iloc[:, i]
        y_pred_s = y_pred_df.iloc[:, i]

        fpr, tpr, _ = roc_curve(y_true_s, y_pred_s)
        auc_score = roc_auc_score(y_true_s, y_pred_s)

        name = f"{y_true_df.columns[i]} (AUC={auc_score:.2f})"
        fig.add_trace(go.Scatter(x=fpr, y=tpr, name=name, mode='lines'))

    fig.update_layout(
        xaxis_title='False Positive Rate',
        yaxis_title='True Positive Rate',
        yaxis=dict(scaleanchor="x", scaleratio=1),
        xaxis=dict(constrain='domain'),
        autosize=False,
        width=size,
        height=size,
    )

    fig.show(renderer)