import tensorflow as tf
import os
import pandas as pd
from datetime import datetime
from functools import partial


#from augmentation import *
# transforms = transformations()


import matplotlib.pyplot as plt
#%matplotlib inline

def view_image(ds):
    image, label = next(iter(ds)) # extract 1 batch from the dataset
    image = image.numpy()
    label = label.numpy()
    
    fig = plt.figure(figsize=(22, 22))
    for i in range(8):
        ax = fig.add_subplot(4, 5, i+1, xticks=[], yticks=[])
        # ax.imshow(image[i].astype(np.uint8))
        # ax.imshow(image[i].astype("int32"))
        ax.imshow(image[i])
        # print(image)
        ax.set_title(f"Label: {label[i]}")


def print_layers_state(model):
    layers = [(layer, layer.name, layer.trainable) for layer in model.layers]
    layer_df = pd.DataFrame(layers, columns=['Layer Type', 'Layer Name', 'Layer Trainable'])
    return layer_df

def cardinality_check(ds):
    num = tf.data.experimental.cardinality(ds).numpy()
    return num

def set_tensorflow_config(per_process_gpu_memory_fraction=0.7):
    config = tf.compat.v1.ConfigProto()
    # config = tf.ConfigProto()
    # config.gpu_options.per_process_gpu_memory_fraction = per_process_gpu_memory_fraction
    config.gpu_options.allow_growth=True
    # sess = tf.Session(config=config)
    sess = tf.compat.v1.Session(config=config)
    print("== TensorFlow Config options set ==")
    print("\nThis process will now utilize {} GPU Memeory Fraction".format(per_process_gpu_memory_fraction))

def split_dataset(list_ds_files, validation_size=0.3, test_size=0.1):
    ## return train, validation and test datasets
    
    image_count = list_ds_files.cardinality().numpy()
    
    dataset = list_ds_files.shuffle(image_count, reshuffle_each_iteration=False)    
    val_size = int(image_count*validation_size)
    test_size = int(image_count*test_size)
    validation_dataset = dataset.take(val_size)
    dataset = dataset.skip(val_size)
    test_dataset = dataset.take(test_size)
    train_dataset = dataset.skip(test_size)
    
    return train_dataset, validation_dataset, test_dataset


# def get_label(is_one_hot, CLASS_NAMES, NUM_CLASSES, file_path):
#     # convert the path to a list of path components
#     parts = tf.strings.split(file_path, os.path.sep)
#     # The second to last is the class-directory
#     bool_label = parts[-2] == CLASS_NAMES
#     # Integer encode the label
#     int_label = tf.argmax(tf.cast(bool_label, tf.int32))
#     # return either one hot or sparse label set
#     if is_one_hot:
#         one_hot_label = tf.one_hot(int_label, NUM_CLASSES)
#         return one_hot_label
#     else:
#         return int_label


# def aug_fn(image, img_size):
#     data = {"image":image}
#     aug_data = transforms(**data)
#     aug_img = aug_data["image"]
#     # aug_img = tf.cast(aug_img/255.0, tf.float32)
#     # aug_img = tf.image.convert_image_dtype(aug_img, tf.float32)
#     aug_img = tf.image.resize(aug_img, size=[img_size, img_size])
#     return aug_img

# def apply_aug(aug_fn, IMG_SIZE, image, label):
#     image = tf.numpy_function(func=aug_fn, inp=[image, IMG_SIZE], Tout=tf.float32)
#     # image = process_aug_image(image, IMG_SIZE, aug_fn)
#     return image, label


# def set_shapes(ONE_HOT, image_size, num_classes, img, label):
#     img_shape=(image_size, image_size, 3)
#     img.set_shape(img_shape)
#     if ONE_HOT:
#         label.set_shape([num_classes])
#     else:
#         label.set_shape([])
#     return img, label
    

# def parse_image(is_train, is_one_hot, CLASS_NAMES, NUM_CLASSES, IMG_SIZE, file_path):
#     label = get_label(is_one_hot, CLASS_NAMES, NUM_CLASSES, file_path)
#     # load the raw data from the file as a string
#     img = tf.io.read_file(file_path)
#     # convert the compressed string to a 3D uint8 tensor
#     img = tf.image.decode_jpeg(img, channels=3)
    
#     # Use `convert_image_dtype` to convert to floats in the [0,1] range
#     # img = tf.image.convert_image_dtype(img, tf.float32)
#     # resize the image to the desired size.
#     if not is_train:
#         img = tf.image.resize(img, [IMG_SIZE, IMG_SIZE])
    

#     return img, label


# def model_preprocess(preprocess_input, image, label):
#     return preprocess_input(image), label



# def data_loader(ds,parse_fun,is_train,is_one_hot,
#                 class_names,num_classes,image_size,autotune, cycle_length, 
#                 batch_size, preprocess_fun, cache_file_name, is_augmentation, shuffle_buffer_size=1000):
    
#     ds = ds.interleave(lambda x: ds, cycle_length=cycle_length)
#     ds = ds.map(partial(parse_fun,is_train,is_one_hot,class_names,num_classes,image_size),num_parallel_calls=autotune)
    
#     # use only when the dataset is small
#     if cache_file_name is not None:
#         ds = ds.cache(cache_file_name)
    
#     if is_augmentation:
#         print("=== Applying Data Augmentation on this dataset ===")
#         ds = ds.map(partial(apply_aug, aug_fn, image_size))
#         ds = ds.map(partial(set_shapes, is_one_hot, image_size, num_classes))
    
#     # include autotune in map here
#     ds = ds.map(partial(model_preprocess, preprocess_fun), num_parallel_calls=autotune)
#     if is_train:
#         print("=== Applying Data Shuffle on this dataset with shuffle buffer size = {} ===".format(shuffle_buffer_size))
#         ds = ds.repeat(50)
        
#         ds = ds.shuffle(buffer_size=shuffle_buffer_size)
#         # I think if you do not repeat this in validation then the pipeline should fail after all the images are over
        
    
#     # this is correct, batching should be done after repeat
#     # once the data is exhausted, repeat will replenish the data
#     ds = ds.batch(batch_size)
#     ds = ds.prefetch(buffer_size=autotune)

#     return ds



def get_loss_and_metrics(is_one_hot, class_type):
    if is_one_hot:
        metrics = [#tf.keras.metrics.CategoricalCrossentropy(),
                   tf.keras.metrics.CategoricalAccuracy()
                  ]
        loss = [tf.keras.losses.CategoricalCrossentropy()]
        metric_to_monitor = 'val_categorical_accuracy'
    else:
        if class_type == 'sparse_categorical':
            metrics = [# tf.keras.metrics.SparseCategoricalCrossentropy(),
                       tf.keras.metrics.SparseCategoricalAccuracy()
                      ]
            loss = [tf.keras.losses.SparseCategoricalCrossentropy()]
            metric_to_monitor = 'val_sparse_categorical_accuracy'
        elif class_type == 'binary':
            metrics = [#tf.keras.metrics.BinaryCrossentropy(),
                       tf.keras.metrics.BinaryAccuracy()
                      ]
            loss = [tf.keras.losses.BinaryCrossentropy()]
            metric_to_monitor = 'val_binary_accuracy'
    return metrics, loss, metric_to_monitor

def model_checkpoint(base_checkpoint_name, checkpoint_dir_name, metrics_to_monitor):
    # Include the epoch in the file name (uses `str.format`)
    checkpoint_filename = '%s_%s.epoch={epoch:02d}-val_acc={%s:.2f}.h5'%(datetime.now().strftime("%d_%m_%Y_%H_%M_%S"), base_checkpoint_name, metrics_to_monitor)
    
#     checkpoint_filename = '%s_%s.epoch={epoch:02d}-val_acc={val_categorical_accuracy:.2f}.h5'%(datetime.now().strftime("%d_%m_%Y_%H_%M_%S"), base_checkpoint_name)

    # dated_checkpoint_filename = format(datetime.now().strftime("%d_%m_%Y_%H_%M_%S"))
    # checkpoint_dir = EXPERIMENT_NAME
    abs_checkpoint_path = os.path.join(checkpoint_dir_name, checkpoint_filename)

    # Create a callback that saves the complete model or it's weights every epoch
    # based on the metric being monitored
    model_checkpoint_callback = tf.keras.callbacks.ModelCheckpoint(
                                                        filepath=abs_checkpoint_path, 
                                                        monitor=metrics_to_monitor, 
                                                        verbose=1,
                                                        save_freq='epoch',
                                                        save_best_only=True, 
                                                        save_weights_only=False, 
                                                        mode='max')
    return model_checkpoint_callback
