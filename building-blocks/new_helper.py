import tensorflow as tf
import os
import pandas as pd
from datetime import datetime
from functools import partial


from albu import *
transforms = transformations()

import matplotlib.pyplot as plt
#%matplotlib inline


def set_tensorflow_config(per_process_gpu_memory_fraction=0.7):
    config = tf.compat.v1.ConfigProto()
    # config = tf.ConfigProto()
    # config.gpu_options.per_process_gpu_memory_fraction = per_process_gpu_memory_fraction
    config.gpu_options.allow_growth=True
    # sess = tf.Session(config=config)
    sess = tf.compat.v1.Session(config=config)
    print("== TensorFlow Config options set ==")
    print("\nThis process will now utilize {} GPU Memeory Fraction".format(per_process_gpu_memory_fraction))



def get_label(file_path, one_hot, CLASS_NAMES, NUM_CLASSES):
    # convert the path to a list of path components
    parts = tf.strings.split(file_path, os.path.sep)
    # The second to last is the class-directory
    bool_label = parts[-2] == CLASS_NAMES
    # Integer encode the label
    int_label = tf.argmax(tf.cast(bool_label, tf.int32))
    # return either one hot or sparse label set
    if one_hot:
        one_hot_label = tf.one_hot(int_label, NUM_CLASSES)
        return one_hot_label
    else:
        return int_label


def preprocess_image_and_label(is_train, one_hot, CLASS_NAMES, NUM_CLASSES, IMG_SIZE, file_path):
    label = get_label(file_path, one_hot, CLASS_NAMES, NUM_CLASSES)
    image = tf.io.read_file(file_path)
    # image = preprocess_image(image)
    image = tf.image.decode_jpeg(image, channels=3)
    # image = tf.image.convert_image_dtype(image, tf.float32)
    if not is_train:
        image = tf.image.resize(image, [IMG_SIZE, IMG_SIZE])
    # image = preprocess_input(image)
    # image = tf.image.convert_image_dtype(image, tf.uint8)
    return image, label

def model_preprocessing(preprocess_input, image, label):
    return preprocess_input(image), label

# def change_dtype(image, label):
#     image = tf.image.convert_image_dtype(image, tf.float32)
#     return image, label


def aug_fn(image, img_size):
    data = {"image":image}
    aug_data = transforms(**data)
    aug_img = aug_data["image"]
    aug_img = tf.image.resize(aug_img, size=[img_size, img_size])
    return aug_img

def process_data(image, label, img_size):
    aug_img = tf.numpy_function(func=aug_fn, inp=[image, img_size], Tout=tf.float32)
    return aug_img, label



def set_shapes(ONE_HOT, image_size, num_classes, img, label):
    img_shape=(image_size, image_size, 3)
    img.set_shape(img_shape)
    if ONE_HOT:
        label.set_shape([num_classes])
    else:
        label.set_shape([])
    return img, label


def check_dataset(dataset):
    for image, label in dataset.take(1):
        print("Image shape: ", image.numpy().shape)
        print("Label: ", label.numpy())



def data_loader(ds, is_train, is_aug, preprocess_input, BATCH_SIZE, AUTOTUNE, ONE_HOT, IMG_SIZE, NUM_CLASSES, CLASS_NAMES, EPOCHS, DATA_BUFFER_SIZE):
    # train_dataset = train_files.interleave(lambda x: tf.data.Dataset.list_files(str(train_data_dir + '/*/*'), shuffle=True), cycle_length=4).map(process_image, num_parallel_calls=tf.data.experimental.AUTOTUNE)
    ds = ds.interleave(lambda x: ds, cycle_length=4)
    ds = ds.map(partial(preprocess_image_and_label, is_train, ONE_HOT, CLASS_NAMES, NUM_CLASSES, IMG_SIZE), num_parallel_calls=AUTOTUNE)
    
    if is_aug:
        ds = ds.map(partial(process_data, img_size=IMG_SIZE),num_parallel_calls=AUTOTUNE)
        ds = ds.map(partial(set_shapes, ONE_HOT, IMG_SIZE, NUM_CLASSES), num_parallel_calls=AUTOTUNE)

    
    #ds = ds.map(change_dtype, num_parallel_calls=AUTOTUNE)
    ds = ds.map(partial(model_preprocessing, preprocess_input), num_parallel_calls=AUTOTUNE)

    # Shuffle and batch the train_dataset. Use a buffer size of 1024
    # for shuffling and a batch size 32 for batching. 
    if is_train:
        ds = ds.repeat(EPOCHS)
        ds = ds.shuffle(buffer_size=DATA_BUFFER_SIZE)
        
        
    ds = ds.batch(BATCH_SIZE)
    # Parallelize the loading by prefetching the train_dataset.
    # Set the prefetching buffer size to tf.data.experimental.AUTOTUNE.
    ds = ds.prefetch(AUTOTUNE)
    
    return ds

def get_loss_and_metrics(is_one_hot, class_type):
    if is_one_hot:
        metrics = [#tf.keras.metrics.CategoricalCrossentropy(),
                   tf.keras.metrics.CategoricalAccuracy()
                  ]
        loss = [tf.keras.losses.CategoricalCrossentropy()]
        metric_to_monitor = 'val_categorical_accuracy'
    else:
        if class_type == 'sparse_categorical':
            metrics = [# tf.keras.metrics.SparseCategoricalCrossentropy(),
                       tf.keras.metrics.SparseCategoricalAccuracy()
                      ]
            loss = [tf.keras.losses.SparseCategoricalCrossentropy()]
            metric_to_monitor = 'val_sparse_categorical_accuracy'
        elif class_type == 'binary':
            metrics = [#tf.keras.metrics.BinaryCrossentropy(),
                       tf.keras.metrics.BinaryAccuracy()
                      ]
            loss = [tf.keras.losses.BinaryCrossentropy()]
            metric_to_monitor = 'val_binary_accuracy'
    return metrics, loss, metric_to_monitor


def view_image(ds):
    image, label = next(iter(ds)) # extract 1 batch from the dataset
    image = image.numpy()
    label = label.numpy()
    
    fig = plt.figure(figsize=(22, 22))
    for i in range(8):
        ax = fig.add_subplot(4, 5, i+1, xticks=[], yticks=[])
        #ax.imshow(image[i].astype(np.uint8))
#         ax.imshow(image[i].astype("int32"))
        #ax.imshow(image[i])
        # print(image)
        ax.set_title(f"Label: {label[i]}")