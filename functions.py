import tensorflow as tf
import os
import pandas as pd
from datetime import datetime
from functools import partial
import matplotlib.pyplot as plt
import numpy as np

#import pickle
import pickle5 as pickle


from augmentation import *
transforms = transformations()


def cardinality_check(ds):
    return ds.cardinality().numpy()


def print_layers_state(model):
    layers = [(layer, layer.name, layer.trainable) for layer in model.layers]
    layer_df = pd.DataFrame(layers, columns=['Layer Type', 'Layer Name', 'Layer Trainable'])
    return layer_df

def split_dataset(list_ds_files, validation_size=0.3, test_size=0.1):
    image_count = list_ds_files.cardinality().numpy()
    dataset = list_ds_files.shuffle(image_count, reshuffle_each_iteration=False)    
    val_size = int(image_count*validation_size)
    test_size = int(image_count*test_size)
    validation_dataset = dataset.take(val_size)
    dataset = dataset.skip(val_size)
    test_dataset = dataset.take(test_size)
    train_dataset = dataset.skip(test_size)
    return train_dataset, validation_dataset, test_dataset


def set_tensorflow_config(per_process_gpu_memory_fraction=0.7):
    config = tf.compat.v1.ConfigProto()
    # config = tf.ConfigProto()
    # config.gpu_options.per_process_gpu_memory_fraction = per_process_gpu_memory_fraction
    config.gpu_options.allow_growth=True
    # sess = tf.Session(config=config)
    sess = tf.compat.v1.Session(config=config)
    print("== TensorFlow Config options set ==")
    # print("\nThis process will now utilize {} GPU Memeory Fraction".format(per_process_gpu_memory_fraction))



def get_label(one_hot, CLASS_NAMES, NUM_CLASSES, file_path):
    # convert the path to a list of path components
    parts = tf.strings.split(file_path, os.path.sep)
    # The second to last is the class-directory
    bool_label = parts[-2] == CLASS_NAMES
    # Integer encode the label
    int_label = tf.argmax(tf.cast(bool_label, tf.int32))
    # return either one hot or sparse label set
    if one_hot:
        one_hot_label = tf.one_hot(int_label, NUM_CLASSES)
        return one_hot_label
    else:
        return int_label

    
# def preprocess_test_image(preprocess_input, path):
#     image = tf.io.read_file(path)
#     image = tf.image.decode_jpeg(image, channels=3)
#     # image = tf.image.convert_image_dtype(image, tf.float32)
#     image = tf.image.resize(image, [IMG_SIZE, IMG_SIZE])
#     image = preprocess_input(image)
#     return image


def preprocess_test_image(preprocess_input, image_size, path):
    image = tf.io.read_file(path)
    image = tf.image.decode_jpeg(image, channels=3)
#     image = tf.image.convert_image_dtype(image, tf.float32)
    image = tf.image.resize(image, [image_size, image_size])
    image = preprocess_input(image)
    return image

def preprocess_image_and_label(is_train, one_hot, CLASS_NAMES, NUM_CLASSES, IMG_SIZE, file_path):
    label = get_label(one_hot, CLASS_NAMES, NUM_CLASSES, file_path)
    image = tf.io.read_file(file_path)
    # image = preprocess_image(image)
    image = tf.image.decode_jpeg(image, channels=3)
    # image = tf.image.convert_image_dtype(image, tf.float32)
    if not is_train:
        image = tf.image.resize(image, [IMG_SIZE, IMG_SIZE])
    # image = preprocess_input(image)
    # image = tf.image.convert_image_dtype(image, tf.uint8)
    return image, label

def model_preprocessing(preprocess_input, image, label):
    return preprocess_input(image), label

def aug_fn(image, img_size):
    data = {"image":image}
    aug_data = transforms(**data)
    aug_img = aug_data["image"]
    # this here added 
    aug_img = tf.cast(aug_img, tf.float32)
    aug_img = tf.image.resize(aug_img, size=[img_size, img_size])
    return aug_img

def process_data(image_size, image, label):
    aug_img = tf.numpy_function(func=aug_fn, inp=[image, image_size], Tout=tf.float32)
    return aug_img, label

def set_shapes(is_one_hot, image_size, num_classes, image, label):
    image_shape=(image_size, image_size, 3)
    image.set_shape(image_shape)
    if is_one_hot:
        label.set_shape([num_classes])
    else:
        label.set_shape([])
    return image, label


def check_dataset(dataset):
    for image, label in dataset.take(1):
        print("Image shape: ", image.numpy().shape)
        print("Label: ", label.numpy())

def data_loader(ds_files, is_train, is_aug, is_preprocess, is_test, is_one_hot, batch_size, image_size, num_classes, class_names, epochs, interleave_cycle_length, autotune, shuffle_buffer_size, preprocess_input):
    
    if not is_test:
        ds_files = ds_files.interleave(lambda x: ds_files, cycle_length=interleave_cycle_length)
        print("Interleave applied")
        
    ds = ds_files.map(partial(preprocess_image_and_label, is_train, is_one_hot, class_names, num_classes, image_size), num_parallel_calls=autotune)
    
    if is_aug:
        ds = ds.map(partial(process_data, image_size), num_parallel_calls=autotune)
        ds = ds.map(partial(set_shapes, is_one_hot, image_size, num_classes))
        print("Augmentation applied")
        
    if is_preprocess:
        ds = ds.map(partial(model_preprocessing, preprocess_input))
        print("Model preprocess_input applied")
    else:
        print("Warn! No model preprocessing applied")
    # Shuffle and batch the train_dataset. Use a buffer size of 1024
    # for shuffling and a batch size 32 for batching. 
    
    if is_train:
        ds = ds.repeat(epochs)
        ds = ds.shuffle(shuffle_buffer_size)
        print("Repeat and shuffle applied")
        
        
    ds = ds.batch(batch_size)
    print("Batch applied")
    
    if not is_test:
        # Parallelize the loading by prefetching the train_dataset.
        # Set the prefetching buffer size to tf.data.experimental.AUTOTUNE.
        ds = ds.prefetch(autotune)
        print("Prefetch applied")
        
    return ds

def get_loss_and_metrics(is_one_hot, class_type):
    if is_one_hot:
        metrics = [#tf.keras.metrics.CategoricalCrossentropy(),
                   tf.keras.metrics.CategoricalAccuracy()
                  ]
        loss = [tf.keras.losses.CategoricalCrossentropy()]
        metric_to_monitor = 'val_categorical_accuracy'
    else:
        if class_type == 'sparse_categorical':
            metrics = [# tf.keras.metrics.SparseCategoricalCrossentropy(),
                       tf.keras.metrics.SparseCategoricalAccuracy()
                      ]
            loss = [tf.keras.losses.SparseCategoricalCrossentropy()]
            metric_to_monitor = 'val_sparse_categorical_accuracy'
        elif class_type == 'binary':
            metrics = [#tf.keras.metrics.BinaryCrossentropy(),
                       tf.keras.metrics.BinaryAccuracy()
                      ]
            loss = [tf.keras.losses.BinaryCrossentropy()]
            metric_to_monitor = 'val_binary_accuracy'
    return metrics, loss, metric_to_monitor


# def view_image(ds):
#     image, label = next(iter(ds)) # extract 1 batch from the dataset
#     image = image.numpy()
#     label = label.numpy()
    
#     fig = plt.figure(figsize=(22, 22))
#     for i in range(8):
#         ax = fig.add_subplot(4, 5, i+1, xticks=[], yticks=[])
#         #ax.imshow(image[i].astype(np.uint8))
# #         ax.imshow(image[i].astype("int32"))
#         #ax.imshow(image[i])
#         # print(image)
#         ax.set_title(f"Label: {label[i]}")
        

        
def view_image(ds, view_albumentations=False):
    image, label = next(iter(ds)) # extract 1 batch from the dataset
    image = image.numpy()
    label = label.numpy()
    
    fig = plt.figure(figsize=(22, 22))
    for i in range(10):
        ax = fig.add_subplot(4, 5, i+1, xticks=[], yticks=[])
        if view_albumentations:
            ax.imshow(image[i])
        else:
            ax.imshow(image[i].astype(np.uint8))
        ax.set_title(f"Label: {label[i]}")
        
def model_checkpoint(base_checkpoint_name, checkpoint_dir_name, metrics_to_monitor):
    # Include the epoch in the file name (uses `str.format`)
    checkpoint_filename = '%s_%s.epoch={epoch:02d}-val_acc={%s:.4f}.h5'%(datetime.now().strftime("%d_%m_%Y_%H_%M_%S"), base_checkpoint_name, metrics_to_monitor)
    
#     checkpoint_filename = '%s_%s.epoch={epoch:02d}-val_acc={val_categorical_accuracy:.2f}.h5'%(datetime.now().strftime("%d_%m_%Y_%H_%M_%S"), base_checkpoint_name)

    # dated_checkpoint_filename = format(datetime.now().strftime("%d_%m_%Y_%H_%M_%S"))
    # checkpoint_dir = EXPERIMENT_NAME
    abs_checkpoint_path = os.path.join(checkpoint_dir_name, checkpoint_filename)

    # Create a callback that saves the complete model or it's weights every epoch
    # based on the metric being monitored
    model_checkpoint_callback = tf.keras.callbacks.ModelCheckpoint(
                                                        filepath=abs_checkpoint_path, 
                                                        monitor=metrics_to_monitor, 
                                                        verbose=1,
                                                        save_freq='epoch',
                                                        save_best_only=True, 
                                                        save_weights_only=False, 
                                                        mode='max')
    return model_checkpoint_callback


def tensorboard(log_dir_path="logs"):
    tensorboard_callback = tf.keras.callbacks.TensorBoard(
    log_dir=log_dir_path,
    write_graph=True,
    write_images=True,
    update_freq="epoch"
)
    return tensorboard_callback


def early_stopping(patience_level=1):
    early_stopping_callback = tf.keras.callbacks.EarlyStopping(monitor='val_loss', 
                                                                   patience=patience_level, 
                                                                   verbose=1, 
                                                                   mode='auto', 
                                                                   restore_best_weights=False)
    return early_stopping_callback



def save_pickle(pickle_object, filename, filepath):
    
    filename = filename+'.pickle'
    abs_pickle_filepath = os.path.join(filepath, filename)
    
    with open(abs_pickle_filepath, 'wb') as handle:
        pickle.dump(pickle_object, handle, protocol=pickle.HIGHEST_PROTOCOL)
        print("Writing to pickle file: {}".format(abs_pickle_filepath))
        

def load_pickle(pickle_file):
    data = None
    with open(pickle_file, 'rb') as handle:
        data = pickle.load(handle)
        
    return data


def evaluation_data_loader(list_ds, preprocess_input, batch_size, image_size, autotune):
    ds = list_ds.map(partial(preprocess_test_image, preprocess_input, image_size), num_parallel_calls=autotune)
    ds = ds.batch(batch_size)
    return ds


def get_file_list(list_ds_files):
    get_filename = lambda x:int(x.decode().split(os.path.sep)[-1].split('.')[0])
    abs_file_paths = list(list_ds_files.as_numpy_iterator())
    filename_list = list(map(get_filename, abs_file_paths))
    
    return filename_list



def combine_filenames_with_predictions(filenames, predictions):
    return list(zip(filenames, list(predictions[:, 1])))



def first(n):
    return n[0]  
   
def custom_sort(tuples):
    return sorted(tuples, key=first)