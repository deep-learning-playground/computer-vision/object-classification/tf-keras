from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Dense, Flatten, Dropout


class smallVGGLikeConvNets(object):
	def __init__(self, num_classes, input_shape=(224, 224, 3), class_mode='binary'):
		"""
		#param
		class_mode: 'binary' or 'categorical' 
		"""
		self.__num_classes = num_classes
		self.__class_mode = class_mode
		self.__input_shape = input_shape
		self.__nn_classifier = ''
		if self.__class_mode == 'binary':
			self.__nn_classifier = 'sigmoid'
		elif self.__class_mode == 'categorical':
			self.__nn_classifier = 'softmax'

	def define_one_block_vgg_model(self):
	    model = Sequential()
	    model.add(Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same', input_shape=self.__input_shape))
	    model.add(MaxPooling2D(2, 2))
	    model.add(Flatten())
	    model.add(Dense(128, activation='relu', kernel_initializer='he_uniform'))
	    model.add(Dense(self.__num_classes, activation=self.__nn_classifier))
	    return model

	def define_two_block_vgg_model(self):
	    model = Sequential()
	    model.add(Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same', input_shape=self.__input_shape))
	    model.add(MaxPooling2D(2, 2))
	    model.add(Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'))
	    model.add(MaxPooling2D(2, 2))
	    model.add(Flatten())
	    model.add(Dense(128, activation='relu', kernel_initializer='he_uniform'))
	    model.add(Dense(self.__num_classes, activation=self.__nn_classifier))
	    return model

	def define_three_block_vgg_model(self):
		model = Sequential()
		model.add(Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same', input_shape=self.__input_shape))
		model.add(MaxPooling2D(2, 2))
		model.add(Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'))
		model.add(MaxPooling2D(2, 2))
		model.add(Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'))
		model.add(MaxPooling2D(2, 2))
		model.add(Flatten())
		model.add(Dense(128, activation='relu', kernel_initializer='he_uniform'))
		model.add(Dense(self.__num_classes, activation=self.__nn_classifier))
		return model

	def define_three_block_vgg_model_with_dropout(self):
	    model = Sequential()
	    model.add(Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same', input_shape=self.__input_shape))
	    model.add(MaxPooling2D(2, 2))
	    model.add(Dropout(0.2))
	    model.add(Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'))
	    model.add(MaxPooling2D(2, 2))
	    model.add(Dropout(0.2))
	    model.add(Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'))
	    model.add(MaxPooling2D(2, 2))
	    model.add(Dropout(0.2))
	    model.add(Flatten())
	    model.add(Dense(128, activation='relu', kernel_initializer='he_uniform'))
	    model.add(Dropout(0.5))
	    model.add(Dense(self.__num_classes, activation=self.__nn_classifier))
	    return model