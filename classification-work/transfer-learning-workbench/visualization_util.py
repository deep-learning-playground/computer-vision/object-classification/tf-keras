import matplotlib.pyplot as plt
# import matplotlib.image as mpimg
from tensorflow.keras.preprocessing.image import load_img
import random
import os
import numpy as np
from helper_functions import standardize_history_keys
import tensorflow

def visualize_dataset_images(image_list, grid_size=4):
    if not(grid_size > 0):
        print('Invalid number chosen')
        sys.exit(1)
    
    nrows = ncols = grid_size
    
    fig = plt.gcf()
    fig.set_size_inches(nrows*grid_size, ncols*grid_size)
    
    rand_image_list = random.choices(image_list, k=grid_size**2)
    
    for i, img_path in enumerate(rand_image_list):
        sp = plt.subplot(nrows, ncols, i+1)
        sp.axis('Off')
        
        #img = mpimg.imread(img_path)
        img = load_img(img_path, target_size=(224, 224))
        plt.title('Class label: {}'.format(os.path.basename(os.path.dirname(img_path))))
        plt.imshow(img)
    
    plt.tight_layout()
    plt.show()


def visualize_incorrect_predictions(df):
	fig = plt.gcf()
	fig.set_size_inches(30, 50)

	for index, row in df.reset_index().iterrows():
		sp = plt.subplot(int(np.ceil(len(df)/4)), 4, index+1)
		sp.axis('Off')

		filepath = row['filepath']
		actual_label = row['actual_label']
		predicted_label = row['predicted_label']

		img = load_img(filepath, target_size=(224, 224))

		plt.title('Actual: {}, Predicted: {}'.format(os.path.basename(os.path.dirname(filepath)), predicted_label))
		plt.imshow(img)
	plt.tight_layout()
	plt.show()


def summarize_diagnostics(history, save_fig=False, figure_name='plot.jpg'):

    std_history = standardize_history_keys(history.history)

    accuracy = std_history['accuracy']
    val_accuracy = std_history['val_accuracy']

    loss = std_history['loss']
    val_loss = std_history['val_loss']

    plt.figure(figsize=(12, 12))
    plt.subplot(2, 1, 1)
    plt.plot(accuracy, label='Training Accuracy')
    plt.plot(val_accuracy, label='Validation Accuracy')
    plt.legend(loc='lower right')
    plt.ylabel('Accuracy')
    plt.ylim([min(plt.ylim()),1])
    plt.title('Training and Validation Accuracy')

    plt.subplot(2, 1, 2)
    plt.plot(loss, label='Training Loss')
    plt.plot(val_loss, label='Validation Loss')
    plt.legend(loc='upper right')
    plt.ylabel('Cross Entropy')
    plt.ylim([0,max(plt.ylim())])
    plt.title('Training and Validation Loss')

    if save_fig:
        plt.savefig(figure_name, dpi=600, quality=90, progressive=True)

    plt.show()    
        

def visualize_feature_maps(obj, model, image_path, num_features=5, only_conv=True):
    image = obj.load_preprocessed_image(obj, image_path)
    successive_outputs = []
    layer_names = []

    for layer in model.layers[1:]:
        if only_conv:
            if 'conv' in layer.name:
                successive_outputs.append(layer.output)
                layer_names.append(layer.name)
        else:
            successive_outputs.append(layer.output)
            layer_names.append(layer.name)

    visualization_model = tensorflow.keras.models.Model(inputs=model.input, outputs=successive_outputs)
    successive_feature_maps = visualization_model.predict(image)


    for layer_name, feature_map in zip(layer_names, successive_feature_maps):
        if len(feature_map.shape) == 4:
            # Just do this for the conv / maxpool layers, not the fully-connected layers
            n_features = feature_map.shape[-1]  # number of features in feature map
            n_features = num_features
            # The feature map has shape (1, size, size, n_features)
            size = feature_map.shape[1]
            # We will tile our images in this matrix

            display_grid = np.zeros((size, size * n_features))

            for i in range(n_features):
                # Postprocess the feature to make it visually palatable
                x = feature_map[0, :, :, i]
                x -= x.mean()
                x /= x.std()
                x *= 64
                x += 128
                x = np.clip(x, 0, 255).astype('uint8')
                # We'll tile each filter into this big horizontal grid
                display_grid[:, i * size : (i + 1) * size] = x


        # Display the grid
        scale = 20. / n_features
        plt.figure(figsize=(scale * n_features, scale))
        plt.title(layer_name)
        plt.grid(False)
        plt.imshow(display_grid, aspect='auto', cmap='viridis')