import json
import cv2
import os, sys
import numpy as np
import random
import matplotlib.pyplot as plt
import datetime
import importlib
import logging
import pandas as pd
import config


import tensorflow
from tensorflow import keras 
from tensorflow.keras.applications import *
from tensorflow.keras.optimizers import *
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.callbacks import ProgbarLogger, TensorBoard, ModelCheckpoint, LearningRateScheduler, Callback, EarlyStopping
from tensorflow.keras.losses import *
from tensorflow.keras.utils import plot_model
from tensorflow.keras.preprocessing.image import load_img, img_to_array
from tensorflow.keras.models import load_model
from tensorflow_config import set_tensorflow_config
# ALLOCATE HOW MUCH GPU IS NEEDED FO;R THE PROCESS
set_tensorflow_config(per_process_gpu_memory_fraction=config.PER_PROCESS_GPU_MEMORY_FRACTION)


import matplotlib.image as mpimg
from IPython.display import display, HTML
from helper_functions import get_available_model_implementations, print_layers_state, get_default_image_size, get_abs_img_path, standardize_history_keys
print(get_available_model_implementations())
from visualization_util import visualize_dataset_images, visualize_incorrect_predictions, summarize_diagnostics, visualize_feature_maps
from custom_callbacks import customCallbacks
# %matplotlib inline
get_ipython().run_line_magic('matplotlib', 'inline')

from transfer_learning import transferLearning