import importlib
import inspect
import tensorflow
from tensorflow import keras
import numpy as np
import pandas as pd
import os
import config

def get_available_model_implementations():
	print('\nCurrent TensorFlow version: {}'.format(tensorflow.__version__))
	print('Current Keras version: {}'.format(keras.__version__))
	print('\n')
	module = importlib.import_module('tensorflow.keras.applications')
	function_list = [function_detail for function_detail in inspect.getmembers(module) if inspect.isfunction(function_detail[1])]
	return list(np.array(function_list)[:,0])


get_abs_img_path = lambda img_dir:[os.path.join(img_dir, i) for i in os.listdir(img_dir)] 


def print_layers_state(model):
    layers = [(layer, layer.name, layer.trainable) for layer in model.layers]
    layer_df = pd.DataFrame(layers, columns=['Layer Type', 'Layer Name', 'Layer Trainable'])
    return layer_df


def get_default_image_size(network_name):
    default_image_size_dict = {
      "densenet121": "",
      "densenet169": "",
      "densenet201": "",
      "inceptionresnetv2": 299,
      "inceptionv3": 299,
      "mobilenet": "",
      "mobilenetv2": "",
      "nasnetlarge": "",
      "nasnetmobile": "",
      "resnet101": 224,
      "resnet101v2": 224,
      "resnet152": 224,
      "resnet152v2": 224,
      "resnet50": 224,
      "resnet50v2": 224,
      "vgg16": 224,
      "vgg19": 224,
      "xception": 299
    }
    
    if network_name not in default_image_size_dict:
        raise AssertionError("Network name could not be determined. Check above for correct network name mapping list.")
    return default_image_size_dict[network_name]


standardize_history_keys = lambda hist_dict: {config.METRICS_MAPPING[i]:hist_dict[i] for i in config.METRICS_MAPPING.keys() if i in hist_dict.keys()}