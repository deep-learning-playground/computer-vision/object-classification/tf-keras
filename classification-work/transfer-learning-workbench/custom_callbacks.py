from tensorflow.keras.callbacks import Callback

class customCallbacks(Callback):
    def on_epoch_end(self, epoch, logs={}):
        if(logs.get('loss')<0.4):
            print('\n == Loss is low so cancelling training! ==')
            self.model.stop_training = True