import tensorflow as tf

def set_tensorflow_config(per_process_gpu_memory_fraction=0.5):
	config = tf.compat.v1.ConfigProto()
	# config = tf.ConfigProto()
	config.gpu_options.per_process_gpu_memory_fraction = per_process_gpu_memory_fraction
	config.gpu_options.allow_growth=True
	# sess = tf.Session(config=config)
	sess = tf.compat.v1.Session(config=config)
	print("== TensorFlow Config options set ==")
	print("\nThis process will now utilize {} GPU Memeory Fraction".format(per_process_gpu_memory_fraction))