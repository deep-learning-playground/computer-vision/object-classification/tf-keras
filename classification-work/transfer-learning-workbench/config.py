import os
import datetime


PER_PROCESS_GPU_MEMORY_FRACTION = 0.8

BASE_DIR_PATH = '/home/acer/Downloads/data/cat_vs_dog_filtered'
TRAIN_DIR_PATH = os.path.sep.join([BASE_DIR_PATH, 'train'])
VALIDATION_DIR_PATH = os.path.sep.join([BASE_DIR_PATH, 'validation'])
TEST_DIR_PATH = os.path.sep.join([BASE_DIR_PATH, 'test'])
VALIDATION_SPILT_PERCENT = 0.20

CLASS_MAPPING = {class_name:idx for idx, class_name in enumerate(os.listdir(TRAIN_DIR_PATH))}
INV_CLASS_MAPPING = dict(map(reversed, CLASS_MAPPING.items()))

NUM_CLASSES = 2
BINARY_CLASSIFICAION_THRESHOLD = 0.5
INIT_LR = 1e-4
MOMENTUM = 0.9
BATCH_SIZE = 32
EPOCHS = 30
METRICS_MAPPING = {"loss":"loss",
                      "val_loss":"val_loss",

                      "categorical_accuracy":"accuracy",
                      "val_categorical_accuracy":"val_accuracy",

                      "binary_accuracy":"accuracy",
                      "val_binary_accuracy":"val_accuracy"
}

BASE_SAVE_PATH = '/home/acer/keras-learnings'

# DIAGNOSTICS_FIGURE_PATH = ''
DIAGNOSTICS_FIGURE_PATH = os.path.join(BASE_SAVE_PATH, 
	'diagnostics_plot_{}.jpg'.format(datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S")))

INTERMEDIATE_WEIGHTS = os.path.join(BASE_SAVE_PATH, 
	'model_weights_tr_{}.h5'.format(datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S")))



# Fine tune variables
FINAL_WEIGHTS = os.path.join(BASE_SAVE_PATH, 
	'model_weights_ft_{}.h5'.format(datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S")))
FINE_TUNE_MODEL = True
FINE_TUNE_EPOCHS = 50
FINE_TUNE_LR = 1e-5