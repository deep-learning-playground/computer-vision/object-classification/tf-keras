"""
Author: Anuj Khandelwal
Email: anujonthemove@gmail.com
"""

import sys, os
import json
import importlib
import pandas as pd
import tensorflow
from tensorflow import keras 
from tensorflow.keras.preprocessing.image import load_img, img_to_array
from tensorflow.keras.applications import *
import numpy as np
from helper_functions import get_available_model_implementations, print_layers_state
from sklearn.metrics import classification_report

class transferLearning(object):
    def __init__(self):
        self.__available_networks = get_available_model_implementations()
        print('Choose networks from the following: ')
        print('\n')
        self.__network_name_dict = {net.lower(): net for net in self.__available_networks}
        print(json.dumps(self.__network_name_dict, indent=2, sort_keys=True))
        
    def __str_to_function_name(self, name):
        return getattr(sys.modules[__name__], name)
    
    def define_model(self, network_name, input_shape, num_classes, activation, class_mode, layer_state=False, use_training=False, fine_tuning=False):
        if network_name not in self.__network_name_dict:
            print('==> Invalid Netowrk Architecture provided. Choose from the above list.')
            print('\nExiting....')
            sys.exit()
        
        
        self.__input_shape = input_shape
        self.__network_name = network_name
        self.preprocess_function = self.__get_preprocess_function()
        
        if class_mode == 'binary':
            self.__get_predicted_class = lambda predictions:list(np.where(predictions>config.BINARY_CLASSIFICAION_THRESHOLD, 1, 0).ravel())
        elif class_mode == 'categorical':
            self.__get_predicted_class = lambda predictions:np.argmax(predictions, axis=1)
            
        
        Network = self.__str_to_function_name(self.__network_name_dict[self.__network_name])
        
        print('\n== Details of model ==')
        print('1.MODEL NAME (network_name): {}'.format(self.__network_name_dict[self.__network_name]))
        print('2.INPUT SHAPE (input_shape): {}'.format(input_shape))
        print('3.NUMBER OF CLASSES (num_classes): {}'.format(num_classes))
        print('4.CLASS MODE (class_mode): {}'.format(class_mode))
        print('5.CLASSIFIER (activation): {}'.format(activation))
        print('6.FINE TUNING (fine_tuning): {}\n'.format(fine_tuning))
        
        # load a pre-trained network, with include_top=False meaning that we removed the classifier part from top
        base_model = Network(include_top=False, weights="imagenet", input_shape=input_shape)
        # freeze all layers of this network - this will serve as a feature extractor 
        base_model.trainable = False
        if layer_state:
            print("\n == Base model layers == ")
            display(print_layers_state(base_model))
        
        
        if use_training:
            # A Keras tensor is a TensorFlow symbolic tensor object
            # It is being created here explicitly so as to pass it to Model() object as inputs
            inputs = tensorflow.keras.Input(shape=input_shape)
            # This is Model object. It groups layers into an object with training and inference features.
            x = base_model(inputs, training=False)
            # at the end, add global average pooling
            x = tensorflow.keras.layers.GlobalAveragePooling2D()(x)
            # we add a classifier layer here
            outputs = tensorflow.keras.layers.Dense(num_classes, activation=activation)(x)
            # and now we give this to Model API 
            model = tensorflow.keras.Model(inputs=inputs, outputs=outputs)
        
        else:
            x = base_model.output
            x = tensorflow.keras.layers.GlobalAveragePooling2D()(x)
            # x = tensorflow.keras.layers.Dropout(0.5)(x)# remove this
            outputs = tensorflow.keras.layers.Dense(num_classes, activation=activation)(x)
            model = tensorflow.keras.Model(inputs=base_model.input, outputs=outputs)
        
        if layer_state:
            print("\n == Custom model layer status ==")
            display(print_layers_state(model))
        if fine_tuning:
            return base_model, model
        else:
            return None, model
        
    def __get_preprocess_function(self):
        _temp = importlib.import_module('tensorflow.keras.applications.'+self.__network_name)
        return getattr(_temp, 'preprocess_input')
        
    @staticmethod
    def load_preprocessed_image(class_object, file_name):
        # load the image
        img = load_img(file_name, target_size=class_object.__input_shape[:2])
        # convert to array
        img = img_to_array(img)
        # reshape into a single sample with 3 channels
        img = np.expand_dims(img, axis=0)
        # preprocess image as per loaded network
        img = class_object.preprocess_function(img)
        return img
    
    
    def process_and_plot_predictions(self, test_generator, predictions, inv_class_mapping, plot=False):
        actual_labels = list(map(lambda str:os.path.basename(os.path.dirname(str)), test_generator.filepaths))
        
        # predicted_class = np.argmax(predictions, axis=1)
        predicted_class = self.__get_predicted_class(predictions)
        predicted_labels = list(map(lambda label:inv_class_mapping[label], predicted_class))
        prediction_df = pd.DataFrame(list(zip(test_generator.filepaths, actual_labels, predicted_labels)), 
                                     columns=['filepath', 'actual_label', 'predicted_label'])
        filtered_df = prediction_df[prediction_df['actual_label'] != prediction_df['predicted_label']]
        
        if plot:
            visualize_incorrect_predictions(filtered_df)
        
        report_df = pd.DataFrame(classification_report(y_true=test_generator.classes, y_pred=np.argmax(predictions, axis=1), 
                                    target_names=test_generator.class_indices.keys(), output_dict=True)).transpose()
        
        return prediction_df, report_df