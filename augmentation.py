import albumentations as A


def transformations():

    transforms = A.Compose([
            # A.Blur(always_apply=False, p=1.0, blur_limit=(3, 6)),
            #A.ChannelDropout(always_apply=False, p=1.0, channel_drop_range=(1, 1), fill_value=102),
            # A.ChannelShuffle(always_apply=False, p=1.0),
            # A.CLAHE(always_apply=False, p=1.0, clip_limit=(1, 5), tile_grid_size=(23, 10)),
            #A.CoarseDropout(always_apply=False, p=1.0, max_holes=8, max_height=8, max_width=8, min_holes=8, min_height=8, min_width=8),
#             A.ColorJitter(),
#             A.Flip(),
#             A.Cutout(),
#             A.FancyPCA(),
#             A.GaussianBlur(),
            #A.GaussNoise(always_apply=False, p=1.0, var_limit=(10.0, 50.0)),
#             A.GlassBlur(),
            A.HorizontalFlip(),
            A.Rotate(limit=25),
#             A.Perspective(),
#             A.Sharpen(),
            #A.ISONoise(always_apply=False, p=1.0, intensity=(0.1, 0.5), color_shift=(0.01, 0.05)),
#             A.MotionBlur(),
            A.RandomBrightnessContrast(always_apply=False, p=1.0, brightness_limit=(0.029999999329447746, 0.2800000011920929), contrast_limit=(-0.1599999964237213, 0.2800000011920929), brightness_by_max=False),
#             A.RandomFog(),
#             A.RandomRain(),
#             A.RandomShadow(),
#             A.RandomSnow(),
#             A.RandomSunFlare(),
#             A.RandomToneCurve(),
#             A.RandomRotate90(),
#             A.Transpose(),
            #A.ShiftScaleRotate(always_apply=False, p=1.0, shift_limit=(-0.26999998092651367, 0.10999999940395355), scale_limit=(-0.10000000149011612, 0.22999998927116394), rotate_limit=(0, 36), interpolation=0, border_mode=0, value=(0, 0, 0), mask_value=None),
#             A.ShiftScaleRotate(shift_limit=0.02, scale_limit=0.02, rotate_limit=10, p=.10),
            #A.OpticalDistortion(always_apply=False, p=1.0, distort_limit=(-0.8700000047683716, 0.85999995470047), shift_limit=(-0.05000000074505806, 0.3799999952316284), interpolation=3, border_mode=3, value=(27, 12, 29), mask_value=None),
            A.GridDistortion(always_apply=False, p=1.0, num_steps=4, distort_limit=(-0.3400000035762787, 0.3199999928474426), interpolation=2, border_mode=4, value=(88, 48, 37), mask_value=None),
#             A.HueSaturationValue(always_apply=False, p=1.0, hue_shift_limit=(-53, 20), sat_shift_limit=(-56, 30), val_shift_limit=(-40, 20)),
            #A.ImageCompression(),
            #A.MultiplicativeNoise(always_apply=False, p=1.0, multiplier=(0.8999999761581421, 1.709999918937683), per_channel=True, elementwise=True),
            #A.RGBShift(always_apply=False, p=1.0, r_shift_limit=(-106, 36), g_shift_limit=(-91, 24), b_shift_limit=(-92, 91)),
            #A.RandomGamma(always_apply=False, p=1.0, gamma_limit=(72, 165), eps=1e-07),
            #A.Equalize(always_apply=False, p=1.0, mode='cv', by_channels=False),
            #A.ElasticTransform(always_apply=False, p=1.0, alpha=0.8100000023841858, sigma=14.09999942779541, alpha_affine=29.529998779296875, interpolation=1, border_mode=1, value=(0, 0, 0), mask_value=None, approximate=False)
        
        ])

    return transforms